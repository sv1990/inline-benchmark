// #define DISABLE_BENCH
#include "benchmark.hh"

#include <boost/timer/timer.hpp>

#include <algorithm>
#include <iostream>
#include <random>
#include <thread>
#include <vector>

int main() {
  // Bench main with boost to compare the impact of the impact of the inline
  // benchmarking statements
  boost::timer::auto_cpu_timer t;

  bench::settings().print_to(std::clog);
  bench::settings().set_order_to(bench::order_by::mean);
  bench::settings().set_time_unit_to(bench::time_unit::microseconds);
  bench::settings().set_total_time_unit_to(bench::time_unit::milliseconds);

  // Benchmark current scope. Use surrounding function as name
  BENCH;
  const auto num_threads = std::thread::hardware_concurrency();
  std::vector<std::thread> threads;
  threads.reserve(num_threads);
  for (std::size_t i = 0; i < num_threads; ++i) {
    // Benchmark current scope. Use filename and line number as name
    BENCH_HERE;
    threads.emplace_back([] {
      std::mt19937 gen{std::random_device{}()};
      std::vector<int> vec(100'000);
      for (std::size_t i = 0; i < 100; ++i) {
        // Benchmark current scope. Pass name as argument.
        BENCH_SCOPE("for-loop");

        // Benchmark a function and pass a name as second argument.
        BENCH_CALL_NAME(
            std::generate(
                begin(vec), end(vec),
                [&] {
                  return std::uniform_int_distribution<int>{0, 9}(gen);
                }),
            "generate");

        // Benchmark a function and use it as a name.
        BENCH_CALL(std::sort(begin(vec), end(vec)));
      }
    });
  }

  // Benchmark an area of the code and pass a label.
  // The end is denoted by BENCH_STOP or by the end of the scope.
  // The label is also used as the name.
  BENCH_START(join);
  for (auto& thread : threads) {
    thread.join();
  }
  BENCH_STOP(join);
}
