#ifndef INLINEBENCHMARK_BENCHMARK_HH_1528718415194606446_
#define INLINEBENCHMARK_BENCHMARK_HH_1528718415194606446_

#include "runner.hh"
#include "settings.hh"

#define BENCH_CONCAT_(a, b) a##b
#define BENCH_CONCAT(a, b) BENCH_CONCAT_(a, b)

#define BENCH_STRINGIFY_(x) #x
#define BENCH_STRINGIFY(x) BENCH_STRINGIFY_(x)

#define BENCH_OBJECT(label) BENCH_CONCAT(local_benchmark_runner_, label)

#ifndef DISABLE_BENCH
#  define BENCH_MACRO(macro) macro
#else
#  define BENCH_MACRO(macro) (void)0
#endif

#define BENCH_SKELETON(label, name)                                            \
  BENCH_MACRO(bench::runner BENCH_OBJECT(label)(name))

#define BENCH_START(label) BENCH_SKELETON(label, BENCH_STRINGIFY(label))
#define BENCH_STOP(label) BENCH_MACRO(BENCH_OBJECT(label).stop())

#define BENCH_SCOPE(name) BENCH_SKELETON(__COUNTER__, name)

#define BENCH BENCH_SCOPE(__PRETTY_FUNCTION__)
#define BENCH_HERE BENCH_SCOPE(__FILE__ ":" BENCH_STRINGIFY(__LINE__))

#ifndef DISABLE_BENCH
#  define BENCH_CALL_NAME(expr, name)                                          \
    [&] {                                                                      \
      BENCH_SCOPE(name);                                                       \
      return (expr);                                                           \
    }()
#else
#  define BENCH_CALL_NAME(expr, name) (expr)
#endif

#define BENCH_CALL(expr) BENCH_CALL_NAME(expr, #expr)

#endif // INLINEBENCHMARK_BENCHMARK_HH_1528718415194606446_
