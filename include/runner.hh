#ifndef INLINE_BENCHMARK_RUNNER_HH_1532680393079364823_
#define INLINE_BENCHMARK_RUNNER_HH_1532680393079364823_

#include <chrono>

namespace bench {
class runner {
  std::chrono::time_point<std::chrono::high_resolution_clock> _start_time;
  const char* _name;
  bool _stopped = false;

public:
  runner(const char* name) noexcept
      : _start_time(std::chrono::high_resolution_clock::now()), _name(name) {}
  void stop() noexcept;
  ~runner();
};
} // namespace bench

#endif // INLINE_BENCHMARK_RUNNER_HH_1532680393079364823_
