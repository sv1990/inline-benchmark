#ifndef INLINE_BENCHMARK_SETTINGS_HH_1532680544791493020_
#define INLINE_BENCHMARK_SETTINGS_HH_1532680544791493020_

#include <iosfwd>
#include <unordered_map>

// __has_builtin(__builtin_unreachable) does not work for gcc
#if defined(__GNUC__) || defined(__clang__)
#  define BENCH_UNREACHABLE __builtin_unreachable();
#else
#  define BENCH_UNREACHABLE
#endif

namespace bench {
enum class order_by {
  name,
  calls,
  total,
  mean,
  min,
  max,
  stddev,
};

enum class time_unit : int {
  nanoseconds = 0,
  microseconds,
  milliseconds,
  seconds,
};

double conversion_factor_from_nano(time_unit t) noexcept;

inline const char* unit_rep(time_unit t) noexcept {
  switch (t) {
  case time_unit::nanoseconds:
    return "[ns]";
  case time_unit::microseconds:
    return "[µs]";
  case time_unit::milliseconds:
    return "[ms]";
  case time_unit::seconds:
    return "[s]";
  }
  BENCH_UNREACHABLE
}

struct entry;

class settings_holder {
  friend class global_accumulator;
  std::ostream* os;
  order_by order;
  time_unit total_time;
  time_unit time;

  settings_holder() noexcept;

public:
  settings_holder(const settings_holder&) = delete;
  settings_holder& operator=(const settings_holder&) = delete;

  settings_holder(settings_holder&&) = delete;
  settings_holder& operator=(settings_holder&&) = delete;

  /**
   * Select the stream where the output should be printed to.
   *
   * The stream object must not have automatic storage duration since it would
   * be destroyed before the output was printed. This is easily prevented by
   * making the stream object global or constructing it via `new` at the cost of
   * a memory leak.
   *
   */
  void print_to(std::ostream& os_) noexcept { os = &os_; }
  void set_order_to(order_by order_) noexcept { order = order_; }
  void set_time_unit_to(time_unit time_) noexcept { time = time_; }
  void set_total_time_unit_to(time_unit total_time_) noexcept {
    total_time = total_time_;
  }

  friend settings_holder& settings() noexcept;
  friend void
  print_results(const std::unordered_map<std::string, entry>& _data);
};

inline settings_holder& settings() noexcept {
  static settings_holder holder;
  return holder;
}
} // namespace bench

#endif // INLINE_BENCHMARK_SETTINGS_HH_1532680544791493020_
