# Inline Benchmark

###### A simple threadsafe C++17 benchmarking library that lets you measure the run-time of selected functions or scopes

## Motivation

When using a profiling tool like gprof it can happen that the profiling output does not show the functions you are interested in.

Another problem with tools like gprof is that they can only show the run-time of function. If you are only interested in a part of the function it is not clear how to achieve that.

This library aims at helping with these issues.

## Caution

Since this is an intrusive benchmarking solution it can alter how the compiler will optimize your code (e.g. what functions will be inlined). If that happens it will less clear how to interpret the results.

## Example

This library consists of the two macros `BENCH` and `BENCH_SCOPE(name)`. The usage is shown below:

```c++
#include "benchmark.hh"
#include <thread>
using namespace std::chrono_literals;

void a() {
  BENCH;
  std::this_thread::sleep_for(50ms);
}

void b() {
  BENCH;
  std::this_thread::sleep_for(25ms);
}

int main() {
  for (int i = 0; i < 10; ++i) {
    BENCH_SCOPE("for-loop");
    a();
    b();
  }
}
```

The output would look like

```bash
Inline benchmark results:
for-loop, 10 calls, 750ms, 75ms/call
void a(), 10 calls, 500ms, 50ms/call
void b(), 10 calls, 250ms, 25ms/call
```

If the macro variable `DISABLE_BENCH` was defined before including `benchmark.hh` the benchmarking macros will expand to no-ops and there will be no performance penalties.

## Usage
The easiest way is to include `inline-benchmark` with your code is to clone this repository in a subdirectory and link with the target `InlineBenchmark::inline_benchmark`. If you have a target `foo` consisting of a source file `main.cpp` it would look like:

```cmake
add_subdirectory(inline-benchmark)
add_executable(foo main.cpp)
target_link_libraries(foo InlineBenchmark::inline_benchmark)
```



## Todos

* Investigate performance effects (especially regarding function inlining)
* Improve CMakeLists.txt
* Option to switch between milli and micro and maybe nanoseconds
* Detect whether the compiler has `__PRETTY_FUNCTION__` and use `__FUNCTION__` instead
* Optional: Include file and line number in the output
