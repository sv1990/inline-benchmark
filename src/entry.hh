#ifndef INLINE_BENCHMARK_ENTRY_HH_1532795003828642706_
#define INLINE_BENCHMARK_ENTRY_HH_1532795003828642706_

#include <chrono>

namespace bench {
using duration_t = std::chrono::nanoseconds;
/**
 * Helper struct for accumulation of runtime data of a function
 */
struct entry {
  std::size_t calls = 1;
  duration_t duration;
  duration_t duration_squared;
  duration_t min_duration;
  duration_t max_duration;
  explicit entry(duration_t duration_) noexcept
      : duration(duration_),
        duration_squared(duration.count() * duration.count()),
        min_duration(duration), max_duration(duration) {}

  void insert(const duration_t& dur) noexcept;
  void combine(const entry& other) noexcept;
};
} // namespace bench

#endif // INLINE_BENCHMARK_ENTRY_HH_1532795003828642706_
