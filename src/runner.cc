#include "runner.hh"

#include "thread_local_accumulator.hh"

namespace bench {
void bench::runner::stop() noexcept {
  const auto end_time = std::chrono::high_resolution_clock::now();
  const auto dur      = std::chrono::duration_cast<std::chrono::milliseconds>(
      end_time - _start_time);
  thread_local_accumulator::get_instance().add(_name, dur);
  _stopped = true;
}

runner::~runner() {
  if (!_stopped) {
    stop();
  }
}
} // namespace bench
