#include "settings.hh"

#include <iostream>

#include <cmath>

namespace bench {
double conversion_factor_from_nano(time_unit t) noexcept {
  return std::pow(10., -3 * static_cast<int>(t));
}

settings_holder::settings_holder() noexcept
    : os(&std::clog), order(order_by::total),
      total_time(time_unit::milliseconds), time(time_unit::microseconds) {
}
} // namespace bench
