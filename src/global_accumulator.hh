#ifndef INLINE_BENCHMARK_GLOBAL_ACCUMULATOR_HH_1532794681718967217_
#define INLINE_BENCHMARK_GLOBAL_ACCUMULATOR_HH_1532794681718967217_

#include "entry.hh"
#include "settings.hh"

#include <mutex>
#include <string>
#include <unordered_map>

#include <cmath>

namespace bench {

/**
 * An global accumulator class that collects the runtime data from the
 * thread-local accumulators and prints it.
 */
class global_accumulator {
  std::unordered_map<std::string, entry> _data;
  std::mutex _mtx;

  global_accumulator() noexcept = default;

public:
  static global_accumulator& get_instance() noexcept {
    static global_accumulator acc;
    return acc;
  }
  void add(std::unordered_map<std::string, entry>&& new_data) noexcept;
  ~global_accumulator();
};
} // namespace bench

#endif // INLINE_BENCHMARK_GLOBAL_ACCUMULATOR_HH_1532794681718967217_
