#include "global_accumulator.hh"

#include "print_results.hh"

namespace bench {

void global_accumulator::add(
    std::unordered_map<std::string, entry>&& new_data) noexcept {
  std::lock_guard lock{_mtx};
  if (_data.empty()) {
    _data = std::move(new_data);
    return;
  }
  for (auto&& [name, entry] : new_data) {
    if (auto it = _data.find(name); it == end(_data)) {
      _data.emplace(std::move(name), std::move(entry));
    }
    else {
      it->second.combine(entry);
    }
  }
}

global_accumulator::~global_accumulator() {
  print_results(_data);
}
} // namespace bench
