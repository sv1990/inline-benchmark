#include "entry.hh"

#include <algorithm>

namespace bench {
void entry::insert(const duration_t& dur) noexcept {
  ++calls;
  duration += dur;
  duration_squared += duration_t{dur.count() * dur.count()};
  min_duration = std::min(min_duration, dur);
  max_duration = std::max(max_duration, dur);
}

void entry::combine(const entry& other) noexcept {
  calls += other.calls;
  duration += other.duration;
  duration_squared += other.duration_squared;
  min_duration = std::min(min_duration, other.min_duration);
  max_duration = std::max(max_duration, other.max_duration);
}

} // namespace bench
