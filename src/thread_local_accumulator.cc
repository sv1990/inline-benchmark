#include "thread_local_accumulator.hh"

#include "global_accumulator.hh"

namespace bench {

void thread_local_accumulator::add(const char* name, duration_t dur) noexcept {
  if (auto it = _data.find(name); it != end(_data)) {
    it->second.insert(dur);
  }
  else {
    _data.emplace(name, dur);
  }
}

thread_local_accumulator::~thread_local_accumulator() {
  global_accumulator::get_instance().add(std::move(_data));
}
} // namespace bench
