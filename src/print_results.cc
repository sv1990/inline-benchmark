#include "print_results.hh"

#include "entry.hh"
#include "settings.hh"

#include <algorithm>
#include <iomanip>
#include <ostream>
#include <unordered_map>
#include <vector>

#include <cmath>

namespace bench {
template <typename Rng, typename Comp, typename Proj>
void sort(Rng& rng, Comp less, Proj proj) noexcept {
  using std::begin, std::end;
  std::sort(begin(rng), end(rng), [=](const auto& l, const auto& r) {
    return less(proj(l), proj(r));
  });
}

template <typename Rng>
void sort_by_order(Rng& data, order_by order) noexcept {
#define PROJECTION(p) [](const auto& x) { return x.p; }
  switch (order) {
  case order_by::name:
    sort(data, std::less<>{}, PROJECTION(name));
    break;
  case order_by::calls:
    sort(data, std::greater<>{}, PROJECTION(calls));
    break;
  case order_by::total:
    sort(data, std::greater<>{}, PROJECTION(total_duration));
    break;
  case order_by::mean:
    sort(data, std::greater<>{}, PROJECTION(mean_duration));
    break;
  case order_by::min:
    sort(data, std::greater<>{}, PROJECTION(min_duration));
    break;
  case order_by::max:
    sort(data, std::greater<>{}, PROJECTION(max_duration));
    break;
  case order_by::stddev:
    sort(data, std::greater<>{}, PROJECTION(stddev));
    break;
  }
#undef PROJECTION
}

/**
 * Helper struct for storing the transformed data
 */
struct entry_result {
  std::string name;
  std::size_t calls;
  duration_t::rep total_duration;
  double mean_duration;
  double stddev;
  duration_t::rep min_duration;
  duration_t::rep max_duration;
  entry_result(const std::pair<std::string, entry>& entry) noexcept
      : name(entry.first), calls(entry.second.calls),
        total_duration(entry.second.duration.count()),
        mean_duration(static_cast<double>(total_duration) /
                      static_cast<double>(calls)),
        stddev(std::sqrt(
            static_cast<double>(entry.second.duration_squared.count()) /
                static_cast<double>(calls) -
            mean_duration * mean_duration)),
        min_duration(entry.second.min_duration.count()),
        max_duration(entry.second.max_duration.count()) {}
};

void print_results(const std::unordered_map<std::string, entry>& _data) {
  auto& os = *bench::settings().os;

  std::vector<entry_result> data(begin(_data), end(_data));
  sort_by_order(data, bench::settings().order);

  const auto longest_name_width =
      static_cast<int>(std::max_element(begin(data), end(data),
                                        [](const auto& l, const auto& r) {
                                          return l.name.size() < r.name.size();
                                        })
                           ->name.size());
  const int name_width = longest_name_width + 2;
  const int num_width  = 14;

  const auto time_unit       = settings().time;
  const auto total_time_unit = settings().total_time;

  const auto time_factor       = conversion_factor_from_nano(time_unit);
  const auto total_time_factor = conversion_factor_from_nano(total_time_unit);

  const auto total_time_rep = unit_rep(total_time_unit);
  const auto time_rep       = unit_rep(time_unit);

  const int num_width_header =
      (time_unit == time_unit::seconds) ? num_width - 3 : num_width - 4;
  const int num_width_header_total =
      (total_time_unit == time_unit::seconds) ? num_width - 3 : num_width - 4;

  os << "\nInline benchmark results:\n";
  os << std::left << std::setw(name_width) << "name"  //
     << std::right << std::setw(num_width) << "calls" //
     << std::right << std::setw(num_width_header_total) << "total "
     << total_time_rep                                                     //
     << std::right << std::setw(num_width_header) << "mean " << time_rep   //
     << std::right << std::setw(num_width_header) << "min " << time_rep    //
     << std::right << std::setw(num_width_header) << "max " << time_rep    //
     << std::right << std::setw(num_width_header) << "stddev " << time_rep //
     << '\n';
  std::fill_n(std::ostreambuf_iterator<char>{os}, name_width + 6 * num_width,
              '-');
  os << '\n';
  for (const auto& [name, calls, total, mean, stddev, min, max] : data) {
    os << std::left << std::setw(name_width) << name                       //
       << std::right << std::setw(num_width) << calls                      //
       << std::right << std::setw(num_width) << total * total_time_factor; //
    if (calls > 1) {
      os << std::right << std::setw(num_width) << mean * time_factor    //
         << std::right << std::setw(num_width) << min * time_factor     //
         << std::right << std::setw(num_width) << max * time_factor     //
         << std::right << std::setw(num_width) << stddev * time_factor; //
    }
    os << "\n";
  }
  os.flush();
}
} // namespace bench
