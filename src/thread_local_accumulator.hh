#ifndef INLINE_BENCHMARK_THREAD_LOCAL_ACCUMULATOR_HH_1532794667722871103_
#define INLINE_BENCHMARK_THREAD_LOCAL_ACCUMULATOR_HH_1532794667722871103_

#include "entry.hh"

#include <chrono>
#include <unordered_map>

namespace bench {

/**
 * An accumulator class that collects the runtime data from a single thread
 * without the need for synchronization. At destruction it moved the data to
 * global_accumulator which collects the data and prints them.
 */
class thread_local_accumulator {
  std::unordered_map<std::string, entry> _data;
  thread_local_accumulator() noexcept = default;

public:
  static thread_local_accumulator& get_instance() noexcept {
    thread_local static thread_local_accumulator acc;
    return acc;
  }
  void add(const char* name, duration_t dur) noexcept;
  ~thread_local_accumulator();
};
} // namespace bench

#endif // INLINE_BENCHMARK_THREAD_LOCAL_ACCUMULATOR_HH_1532794667722871103_
