#ifndef INLINE_BENCHMARK_SRC_PRINT_RESULTS_HH_1535976933220000101_
#define INLINE_BENCHMARK_SRC_PRINT_RESULTS_HH_1535976933220000101_

#include <string>
#include <unordered_map>

namespace bench {
struct entry;
void print_results(const std::unordered_map<std::string, entry>& _data);

} // namespace bench

#endif // INLINE_BENCHMARK_SRC_PRINT_RESULTS_HH_1535976933220000101_
