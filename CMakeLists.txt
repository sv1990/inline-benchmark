cmake_minimum_required(VERSION 3.8)

project(inline_benchmark CXX)

if(NOT CMAKE_BUILD_TYPE)
  set(CMAKE_BUILD_TYPE Release)
endif()
set(USE_THREAD_SANITIZER FALSE)

add_subdirectory(src)

set(CMAKE_THREAD_PREFER_PTHREAD TRUE)
set(THREADS_PREFER_PTHREAD_FLAG TRUE)
find_package(Threads REQUIRED)
find_package(Boost REQUIRED COMPONENTS timer)

add_executable(example example.cc)
target_link_libraries(example Threads::Threads inline_benchmark Boost::timer)
if (CMAKE_CXX_COMPILER_ID MATCHES "Clang|GNU")
  target_compile_options(example PRIVATE -Wall -Wextra -pedantic)
endif()
if (USE_THREAD_SANITIZER)
  target_compile_options(example PRIVATE -fsanitize=thread)
  set_target_properties(example PROPERTIES LINK_FLAGS -fsanitize=thread)
endif ()
